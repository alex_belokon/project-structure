﻿using System;
using Structure.Core.Interfaces.Repository;

namespace Structure.Core.Models
{
    public class Team : IEntity
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
