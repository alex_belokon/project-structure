﻿using System;
using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;

namespace Structure.Core.Models
{
    public class Project : IEntity
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public User User { get; set; }
        public Team Team { get; set; }
        public List<Task> ProjectTasks { get; set; }
    }
}
