﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Business
{
    public interface IUserManager
    {
        User[] GetUsers();
        void Create(User user);
        void Update(User user);
        void Delete(User user);
    }
}
