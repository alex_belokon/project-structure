﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Business
{
    public interface ITeamManager
    {
        Team[] GetTeams();
        void Create(Team team);
        void Update(Team team);
        void Delete(Team team);
    }
}
