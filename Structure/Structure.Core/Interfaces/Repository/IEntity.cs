﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.Core.Interfaces.Repository
{
    public interface IEntity
    {
        int Id { get; }
    }
}
