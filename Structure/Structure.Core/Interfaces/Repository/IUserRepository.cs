﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Repository
{
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
}
