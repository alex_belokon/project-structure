﻿using System.Collections.Generic;

namespace Structure.Core.Interfaces.Repository
{
    public interface IRepositoryBase<T>
    {
        IEnumerable<T> FindAll();
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
