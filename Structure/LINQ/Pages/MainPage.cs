﻿using EasyConsole;

namespace LINQ.Pages
{
    class MainPage : MenuPage
    {
        public MainPage(Program program)
            : base("Main Page", program,
                  new Option("Start Page", () => program.NavigateTo<StartPage>()),
                  new Option("Exit", () => program.NavigateTo<Exit>()))
        {
        }
    }
}
