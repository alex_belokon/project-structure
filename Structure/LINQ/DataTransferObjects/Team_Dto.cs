﻿using Structure.Core.Models;

namespace LINQ.DataTransferObjects
{
    public class Team_Dto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public User User { get; set; }
    }
}
