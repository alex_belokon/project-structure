﻿using Structure.Core.Models;

namespace LINQ.DataTransferObjects
{
    public struct Response_Dto
    {
        public User User { get; set; }
        public Project Project { get; set; }
        public int CountOfTasks { get; set; }
        public int CountOfCanceledTasks { get; set; }
        public Task LongTask { get; set; }
    }
}
