﻿using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using System.Collections.Generic;

namespace Structure.Data.Repository
{
    public class ProjectRepository : RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(List<Project> dataContext) : base(dataContext)
        { }
    }
}
