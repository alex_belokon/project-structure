﻿using Structure.Core.Models;
using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;

namespace Structure.Data.Repository
{
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public TeamRepository(List<Team> dataContext) : base(dataContext)
        { }
    }
}
