﻿using Structure.Core.Interfaces.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Structure.Data.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class, IEntity
    {
        private List<T> dataContext;

        public RepositoryBase(List<T> dataContext)
        {
            this.dataContext = dataContext;
        }
        
        public IEnumerable<T> FindAll()
        {
            return this.dataContext;
        }

        public void Create(T entity)
        {
            //this.dataContext.Set<T>().Add(entity);
            this.dataContext.Add(entity);
        }

        public void Update(T entity)
        {
            //this.dataContext.Set<T>().Update(entity);
            var oldEntity = this.dataContext.SingleOrDefault(p => p.Id == entity.Id);
            if (oldEntity != null)
            {
                this.dataContext.Remove(oldEntity);
                this.dataContext.Add(entity);
            }
        }

        public void Delete(T entity)
        {
            //this.dataContext.Set<T>().Remove(entity);
            this.dataContext.Remove(entity);
        }
    }
}
