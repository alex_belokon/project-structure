﻿using Structure.Core.Models;
using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;

namespace Structure.Data.Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(List<User> dataContext) : base(dataContext)
        { }
    }
}
