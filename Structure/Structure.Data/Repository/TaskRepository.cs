﻿using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;

namespace Structure.Data.Repository
{
    public class TaskRepository : RepositoryBase<Task>, ITaskRepository
    {
        public TaskRepository(List<Task> dataContext) : base(dataContext)
        { }
    }
}
