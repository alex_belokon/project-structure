using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Structure.Business.Managers;
using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Data.Repository;
using Structure.WebAPI.Service;
using System;
using System.Linq;

namespace Structure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var projects = services.AddTransient<JsonFileProjectService>().BuildServiceProvider().GetService<JsonFileProjectService>().GetProjects().ToList();
            var teams = services.AddTransient<JsonFileTeamService>().BuildServiceProvider().GetService<JsonFileTeamService>().GetTeams().ToList();
            var users = services.AddTransient<JsonFileUserService>().BuildServiceProvider().GetService<JsonFileUserService>().GetUsers().ToList();
            var tasks = services.AddTransient<JsonFileTaskService>().BuildServiceProvider().GetService<JsonFileTaskService>().GetTasks().ToList();

            services.AddScoped<IProjectRepository>(x => new ProjectRepository(projects));
            services.AddScoped<ITeamRepository>(x => new TeamRepository(teams));
            services.AddScoped<IUserRepository>(x => new UserRepository(users));
            services.AddScoped<ITaskRepository>(x => new TaskRepository(tasks));

            services.AddScoped<IProjectManager, ProjectManager>();
            services.AddScoped<ITeamManager, TeamManager>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<ITaskManager, TaskManager>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
