﻿using Microsoft.AspNetCore.Mvc;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskManager _taskManager;
        public TasksController(ITaskManager taskManager)
        {
            _taskManager = taskManager;
        }
        // GET: api/Tasks
        [HttpGet]
        public IActionResult Get()
        {
            var tasks = _taskManager.GetTasks();
            return Ok(tasks);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Task task)
        {
            _taskManager.Create(task);

            return Created("URI of the created entity", task);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Task task)
        {
            _taskManager.Update(task);
            return Ok(task);
        }

        [HttpDelete]
        public IActionResult Delete([FromBody] Task task)
        {
            _taskManager.Delete(task);

            return NoContent();
        }
    }
}

