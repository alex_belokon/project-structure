﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserManager _userManager;
        public UsersController(IUserManager userManager)
        {
            _userManager = userManager;
        }
        // GET: api/Users
        [HttpGet]
        public IActionResult Get()
        {
            var users = _userManager.GetUsers();
            return Ok(users);
        }

        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            _userManager.Create(user);

            return Created("URI of the created entity", user);
        }

        [HttpPut]
        public IActionResult Put([FromBody] User user)
        {
            _userManager.Update(user);
            return Ok(user);
        }

        [HttpDelete]
        public IActionResult Delete([FromBody] User user)
        {
            _userManager.Delete(user);

            return NoContent();
        }
    }
}

