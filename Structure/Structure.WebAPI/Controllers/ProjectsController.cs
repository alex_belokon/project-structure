﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectManager _projectManager;
        public ProjectsController(IProjectManager projectManager)
        {
            _projectManager = projectManager;
        }
        // GET: api/Projects
        [HttpGet]
        public IActionResult Get()
        {
            var projects = _projectManager.GetProjects();
            return Ok(projects);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Project project)
        {
            _projectManager.Create(project);

            return Created("URI of the created entity", project);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Project project)
        {
            _projectManager.Update(project);
            return Ok(project);
        }

        [HttpDelete]
        public IActionResult Delete([FromBody] Project project)
        {
            _projectManager.Delete(project);

            return NoContent();
        }
    }
}
