﻿using Microsoft.AspNetCore.Mvc;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamManager _teamManager;
        public TeamsController(ITeamManager teamManager)
        {
            _teamManager = teamManager;
        }
        // GET: api/Teams
        [HttpGet]
        public IActionResult Get()
        {
            var teams = _teamManager.GetTeams();
            return Ok(teams);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Team team)
        {
            _teamManager.Create(team);

            return Created("URI of the created entity", team);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Team team)
        {
            _teamManager.Update(team);
            return Ok(team);
        }

        [HttpDelete]
        public IActionResult Delete([FromBody] Team team)
        {
            _teamManager.Delete(team);

            return NoContent();
        }
    }
}

