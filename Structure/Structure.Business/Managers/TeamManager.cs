﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;

namespace Structure.Business.Managers
{
    public class TeamManager : ITeamManager
    {
        private readonly ITeamRepository teamRepository;
        public TeamManager(ITeamRepository teamRepository)
        {
            this.teamRepository = teamRepository;
        }
        public Team[] GetTeams()
        {
            return this.teamRepository.FindAll().ToArray();
        }
        public void Create(Team team)
        {
            this.teamRepository.Create(team);
        }

        public void Update(Team team)
        {
            this.teamRepository.Update(team);
        }

        public void Delete(Team team)
        {
            this.teamRepository.Delete(team);
        }
    }
}
