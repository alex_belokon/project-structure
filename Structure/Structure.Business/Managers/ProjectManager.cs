﻿using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using System.Linq;

namespace Structure.Business.Managers
{
    public class ProjectManager : IProjectManager
    {
        private readonly IProjectRepository projectRepository;
        public ProjectManager(IProjectRepository projectRepository)
        {
            this.projectRepository = projectRepository;
        }
        public Project[] GetProjects()
        {
            return this.projectRepository.FindAll().ToArray();
        }

        public void Create(Project project)
        {
            this.projectRepository.Create(project);
        }

        public void Update(Project project)
        {
            this.projectRepository.Update(project);
        }

        public void Delete(Project project)
        {
            this.projectRepository.Delete(project);
        }
    }
}
