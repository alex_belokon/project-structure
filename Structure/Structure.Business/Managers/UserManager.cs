﻿using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using System.Linq;

namespace Structure.Business.Managers
{
    public class UserManager : IUserManager
    {
        private readonly IUserRepository userRepository;
        public UserManager(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }
        public User[] GetUsers()
        {
            return this.userRepository.FindAll().ToArray();
        }
        public void Create(User user)
        {
            this.userRepository.Create(user);
        }

        public void Update(User user)
        {
            this.userRepository.Update(user);
        }

        public void Delete(User user)
        {
            this.userRepository.Delete(user);
        }
    }
}
